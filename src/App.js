import React from 'react';
import './App.css';

// Import npm components
import { BrowserRouter, Switch, Route } from "react-router-dom";

// Import custom pages and components
import HomePage from './Pages/HomePage/Homepage';
import ButtonsPage from './Pages/ButtonsPage/ButtonsPage';
import CheckboxesPage from './Pages/CheckboxesPage/CheckboxesPage';
import ModalPage from './Pages/ModalPage/ModalPage';


const App = () => {
    return (
        <div className="c-header">
            <h1 className="c-header-title">React <span>UI Kit</span></h1>
            
            <BrowserRouter>
                <Switch>
                    <Route exact path="/"           render={() => <HomePage />       } />
                    <Route exact path="/btnspage"   render={() => <ButtonsPage />    } />
                    <Route exact path="/chkbxpage" render={() => <CheckboxesPage /> } />
                    <Route exact path="/mdlpage"   render={() => <ModalPage/>       } />
                </Switch>
            </BrowserRouter>
        </div>
    );
};

export default App;


