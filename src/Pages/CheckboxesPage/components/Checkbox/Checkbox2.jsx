import React from 'react';
import './Checkbox2.css';

const Checkbox2 = props => {
    return (
        <div className={`c-checkbox2 ${props.background ? props.background : ''}`}
             onClick={props.btnClick}>
                 <div className={`c-checkbox2-border ${props.border ? props.border : ''}`}></div>

        </div>
    );
}

export default Checkbox2;