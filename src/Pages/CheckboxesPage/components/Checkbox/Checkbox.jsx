import React, { useState } from 'react';
import './Checkbox.css';

export default function() {
    
    const [selected, setSelected] = useState(false);

    return (
        <div className="c-checkbox" onClick={() => {setSelected(!selected);}}>
            <div className={"c-checkbox-border" + (selected ? " bordered" : "")}></div>
            <div className={"c-checkbox-mark" + (selected ? " animation" : "")}></div>         
        </div>        
    );
};
