import React from 'react';
import './Button.css';

class Button extends React.Component {
    render() {
        return (
            <button className={`c-button ${this.props.className ? this.props.className : ''}`}
                    onClick={this.props.btnClick}>
                    {this.props.btnText}
            </button>
        );
    };
};

export default Button;