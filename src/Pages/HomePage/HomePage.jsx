import React from 'react';
import './Homepage.css';

// Import npm components


// Import custom pages and components
import NavBar   from '../../CommonComponents/NavBar/NavBarComponent';

export default function() {
    return (
        <div className="c-main-area">
            <NavBar/>

        </div>
    );
};