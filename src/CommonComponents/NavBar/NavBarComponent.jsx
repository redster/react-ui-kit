import React from 'react';
import './NavBarComponent.css'

// Import npm components
import { NavLink } from 'react-router-dom';

const NavBarComponent = () => {
    return (        
        <div className="c-navbar-links">
            <NavLink to="/btnspage"    className="c-navbar-link">Buttons</NavLink>
            <NavLink to="/chkbxpage"   className="c-navbar-link">Checkboxex</NavLink>
            <NavLink to="/mdlpage"     className="c-navbar-link">Modal</NavLink>
        </div>        
    );    
};

export default NavBarComponent;
